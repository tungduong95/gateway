const express = require("express");
const User = require("../models/User");

const router = express.Router();

router.get("/", () => console.log("Welcome to contact!"));

router.post("/:id", (req, res) => {
  const toEmail = { email: req.body.toEmail };
  User.findOneAndUpdate(
    { _id: req.body._id },
    { $push: { contacts: { username: req.body.email, _id:req.body.id1} } },
    { new: true },
    (err, doc) => {
      if (err) {
        console.log(err);
      }
      console.log(doc);
    }
  );
});

module.exports = router;
