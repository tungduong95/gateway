const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const secretKey = require('../config')

router.get('/',(req,res)=>{
    res.status(200).json({messgage:"Welcome to login"})
})

router.post('/',(req,res)=>{//dang nhap
    User.findOne({"email":req.body.email})
    .then(result=>{
        if(result){
            bcrypt.compare(req.body.password,result.password,(err,isVerified)=>{//Hash mk
                if(!isVerified){
                    res.status(400).json({message:'Invalid Username or Password'})
                }
                else{
                const token = jwt.sign({email:req.body.email},secretKey,(err,token)=>{
                    User
                    .findByIdAndUpdate(result._id,{accessToken:token,status:'Online'})
                    .then(()=>res.status(200).json({message:`token created for user ${result.email}`, token: token}))
                    .catch(error=>console.log(error))
                    }
                )}
            })
        }
        else{
            res.status(400).json({message:'Invalid Username or Password'})
        }
    })
    .catch(()=>res.status(400).json({message:'Invalid Username or Password'}))
})

module.exports = router;