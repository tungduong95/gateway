const mongoose = require('mongoose');
const { Timestamp } = require('mongodb');
const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    name: { type: String, required: true },
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true, select: false },
    email: { type: String, required: true, unique: true },
    accessToken: { type: String },
    status: { type: String },
    friends: [
      {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
      },
      status: {type: Number}
      }],
      message:[{
        users: [{type: Schema.Types.ObjectId,
          ref: 'User'}],
        content:[]     
        },{timestamps:true}
      ]
  },
  { timestamps: true }
);


module.exports = mongoose.model('User', UserSchema);
